#pragma once
#include <iostream>
#include <string>

using namespace std;

class TicTacToe	
{

private:

	char m_board[9] = { '1', '2', '3', '4', '5', '6', '7', '8', '9' };
	int m_numTurns = 10;
	char m_playerTurn;
	char m_winner;
	int ChangePlayer = 1;
	bool SpotTaken[9] = { false };
	bool Winner = false;
	bool TieGame = false;


public:

	TicTacToe()
	{
		char m_board[9];
		int m_numTurns;
		char m_playerTurn;
		int ChangePlayer = 1;
	}

	bool IsOver()
	{
		if (m_board[0] == 'X' &&  m_board[1] == 'X' && m_board[2] == 'X')
		{
			return Winner = true;
		}
		else if (m_board[3] == 'X' && m_board[4] == 'X' && m_board[5] == 'X')
		{
			return Winner = true;
		}
		else if (m_board[6] == 'X' && m_board[7] == 'X' && m_board[8] == 'X')
		{
			return Winner = true;
		}
		else if (m_board[0] == 'X' && m_board[4] == 'X' && m_board[8] == 'X')
		{
			return Winner = true;
		}
		else if (m_board[2] == 'X' && m_board[4] == 'X' && m_board[6] == 'X')
		{
			return Winner = true;
		}
		else if (m_board[0] == 'X' && m_board[3] == 'X' && m_board[6] == 'X')
		{
			return Winner = true;
		}
		else if (m_board[1] == 'X' && m_board[4] == 'X' && m_board[7] == 'X')
		{
			return Winner = true;
		}
		else if (m_board[2] == 'X' && m_board[5] == 'X' && m_board[8] == 'X')
		{
			return Winner = true;
		}
		else if (m_board[0] == 'O' && m_board[1] == 'O' && m_board[2] == 'O')
		{
			return Winner = true;
		}
		else if (m_board[3] == 'O' && m_board[4] == 'O' && m_board[5] == 'O')
		{
			return Winner = true;
		}
		else if (m_board[6] == 'O' && m_board[7] == 'O' && m_board[8] == 'O')
		{
			return Winner = true;
		}
		else if (m_board[0] == 'O' && m_board[4] == 'O' && m_board[8] == 'O')
		{
			return Winner = true;
		}
		else if (m_board[2] == 'O' && m_board[4] == 'O' && m_board[6] == 'O')
		{
			return Winner = true;
		}
		else if (m_board[0] == 'O' && m_board[3] == 'O' && m_board[6] == 'O')
		{
			return Winner = true;
		}
		else if (m_board[1] == 'O' && m_board[4] == 'O' && m_board[7] == 'O')
		{
			return Winner = true;
		}
		else if (m_board[2] == 'O' && m_board[5] == 'O' && m_board[8] == 'O')
		{
			return Winner = true;
		}
		else if (m_numTurns < 1)
		{
			return TieGame = true;
		}
		else
		{
			return false;
		}
	}

	char GetPlayerTurn()
	{

		char Player1 = 'X';
		char Player2 = 'O';

		if (ChangePlayer == 1)
		{
			ChangePlayer++;
		}
		else if (ChangePlayer == 2)
		{
			ChangePlayer--;
		}
		if (ChangePlayer == 1)
		{
			m_playerTurn = Player2;
		}
		if (ChangePlayer == 2)
		{
			m_playerTurn = Player1;
		}
		return m_playerTurn;
	}

	bool IsValidMove(int position)
	{
		if (SpotTaken[position - 1] == false)
		{
			return true;
		}
		else
		{
			cout << "Invalid Move, try again" << "\n";

			if (m_playerTurn = 'X')
			{
				ChangePlayer ++;
			}
			else
			{
				ChangePlayer --;
			}

			return false;
		}
	}

	void Move(int position)
	{

		if (position == 1 && SpotTaken[0] == false)
		{
			m_board[position - 1] = m_playerTurn;
			SpotTaken[0] = true;
		}
		else if (position == 2 && SpotTaken[1] == false)
		{
			m_board[position - 1] = m_playerTurn;
			SpotTaken[1] = true;
		}
		else if (position == 3 && SpotTaken[2] == false)
		{
			m_board[position - 1] = m_playerTurn;
			SpotTaken[2] = true;
		}
		else if (position == 4 && SpotTaken[3] == false)
		{
			m_board[position - 1] = m_playerTurn;
			SpotTaken[3] = true;
		}
		else if (position == 5 && SpotTaken[4] == false)
		{
			m_board[position - 1] = m_playerTurn;
			SpotTaken[4] = true;
		}
		else if (position == 6 && SpotTaken[5] == false)
		{
			m_board[position - 1 ] = m_playerTurn;
			SpotTaken[5] = true;
		}
		else if (position == 7 && SpotTaken[6] == false)
		{
			m_board[position - 1] = m_playerTurn;
			SpotTaken[6] = true;
		}
		else if (position == 8 && SpotTaken[7] == false)
		{
			m_board[position - 1] = m_playerTurn;
			SpotTaken[7] = true;
		}
		else if (position == 9 && SpotTaken[8] == false)
		{
			m_board[position - 1] = m_playerTurn;
			SpotTaken[8] = true;
		}
	}
	
	void DisplayResult()
	{
		if (Winner == true)
		{
			cout << "Player " << m_playerTurn << " is the winner!" << "\n";
		}
		else if (TieGame = true)
		{
			cout << "Tie Game!" << "\n";
		}
		else
		{
			IsOver();
		}
	}

	void DisplayBoard()
	{
		cout << "\n";
		cout << "	" << m_board[0] << "	|	" << m_board[1] << "	|	" << m_board[2] << "\n";
		cout << "      _____________________________________" << "\n";
		cout << "	" << m_board[3] << "	|	" << m_board[4] << "	|	" << m_board[5] << "\n";
		cout << "      _____________________________________" << "\n";
		cout << "	" << m_board[6] << "	|	" << m_board[7] << "	|	" << m_board[8] << "\n" << "\n";
		m_numTurns--;
	}

};
